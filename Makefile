MAKEFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
X_ROOT := $(subst /Makefile,,$(MAKEFILE_PATH))
UNAME := $(if $(USERPROFILE),win32-x86-64,linux-x86-64)
MY_MILL_OPTION := $(if $(USERPROFILE),-i,-i)
MY_MILL_PREFIX := $(if $(USERPROFILE),.\\,./)
#NEMU_LITE_HOME := $(ROOT)/nemu-lite
#CMAKE_BINARY_DIR := $(NEMU_LITE_HOME)/build
CMAKE_BINARY_DIR := $(X_ROOT)/build
# CMAKE_INSTALL_PREFIX := $(X_ROOT)/src/test/resources/$(UNAME)
CMAKE_INSTALL_PREFIX := $(X_ROOT)/src/test/resources
WIN32 := $(if $(USERPROFILE),true,)
#$(shell echo "Platform $(if $(WIN32),Windows,Linux)")
DEFAULT_TEST := mill
PYTHON_EXEC := $(if $(USERPROFILE),python,python3)
SBT := $(if $(WIN32),sbt-win.bat,./sbt -sbt-version 1.6.0)
PYTHON_EXEC := $(if $(WIN32),python,python3)

SYSTEM_MODULE := System
RTL_PATH := $(CMAKE_BINARY_DIR)/chisel-rtl
SYSTEM_RTL := $(RTL_PATH)/$(SYSTEM_MODULE).v

#EDA := pds
EDA := vivado

cmake-build:
	cmake -S . -B $(CMAKE_BINARY_DIR) -G Ninja -D CMAKE_BUILD_TYPE=Debug
	cmake --build build
	cmake --install $(CMAKE_BINARY_DIR) --prefix $(CMAKE_INSTALL_PREFIX)

cmake-release-build:
	cmake -S . -B $(CMAKE_BINARY_DIR) -G Ninja -D CMAKE_BUILD_TYPE=Release
	cmake --build build
	cmake --install $(CMAKE_BINARY_DIR) --prefix $(CMAKE_INSTALL_PREFIX)

release: cmake-release-build

cmake-just-build:
	cmake -S . -B $(CMAKE_BINARY_DIR) -G Ninja -D CMAKE_BUILD_TYPE=Debug -D IGNORE_SCALA_UPDATE=ON
	cmake --build build
	cmake --install $(CMAKE_BINARY_DIR) --prefix $(CMAKE_INSTALL_PREFIX)

build:
	cmake --build build

menuconfig:
	$(shell echo python$(if $(WIN32),,3) -m menuconfig)
#	-$(shell $(if $(WIN32),cmd /k copy,cp) $(X_ROOT)/.config $(X_ROOT)/.temp/config)

generate: sbt-file
	cmake -S . -B $(CMAKE_BINARY_DIR) -G Ninja -D CMAKE_INSTALL_PREFIX=$(CMAKE_INSTALL_PREFIX) -D CMAKE_BUILD_TYPE=Debug -D BATCH_MODE=ON -D SHOW_CONFIG=ON

nemu:
	cmake --build $(CMAKE_BINARY_DIR) -j
	cmake --install $(CMAKE_BINARY_DIR) --prefix $(CMAKE_INSTALL_PREFIX)

ci: generate nemu

idea:
	$(if $(WIN32),,chmod +x millw)
	$(MY_MILL_PREFIX)millw $(MY_MILL_OPTION) mill.scalalib.GenIdea/idea
	# Use Backup misc.xml
	$(shell $(if $(WIN32),cmd /k copy,cp) $(X_ROOT)/scripts/idea-misc.xml $(X_ROOT)/.idea/misc.xml)
	#$(if $(WIN32),cmd /k copy,cp) $(X_ROOT)/scripts/idea-misc.xml $(X_ROOT)/.idea/misc.xml

all-test: mill-test mill-test

ci-test: all-test

sbt-file:
	$(if $(WIN32),,chmod +x sbt)

sbt-test: sbt-file
	 -v "test"

generate-config: clean-config
	-$(PYTHON_EXEC) -m genconfig --config-out .config --header-path $(CMAKE_BINARY_DIR)/include/generated/autoconf.h
	-$(PYTHON_EXEC) -m genconfig --config-out .console.config --header-path $(CMAKE_BINARY_DIR)/include/generated/autoconf.h

mill-test:
	$(if $(WIN32),,chmod +x millw)
	$(MY_MILL_PREFIX)millw $(MY_MILL_OPTION) __.test

ctest: cmake-build
	ctest --test-dir $(CMAKE_BINARY_DIR)

default-test: $(DEFAULT_TEST)-test

test: default-test

verilog: sbt-file
	$(SBT) run

vivado-create:
	echo "creating vivado projects for modules: $(MODULES)"
	$(PYTHON_EXEC) scripts/vivado.py -c $(MODULES) $(PY_ARGS)

vivado-synth:
	echo "synth vivado modules: $(MODULES)"
	$(PYTHON_EXEC) scripts/vivado.py -s -t $(MODULES) $(PY_ARGS)

vivado-impl:
	echo "impl vivado modules: $(MODULES)"
	$(PYTHON_EXEC) scripts/vivado.py -i -t $(MODULES) $(PY_ARGS)

pds-create:
	echo "creating pds projects for modules: $(MODULES)"
	$(PYTHON_EXEC) scripts/pds.py -c $(MODULES) $(PY_ARGS)

pds-synth:
	echo "synth pds modules: $(MODULES)"
	$(PYTHON_EXEC) scripts/pds.py -s -t $(MODULES) $(PY_ARGS)

pds-impl:
	echo "impl pds modules: $(MODULES)"
	$(PYTHON_EXEC) scripts/pds.py -i -t $(MODULES) $(PY_ARGS)

pds-prog:
	echo "program pds modules: $(MODULES)"
	cat `pwd`/scripts/tcl-pds/prog.tcl | sed s/%top%/$(MODULES)/g > pds/$(MODULES)/prog.tcl
	cd pds/$(MODULES) && cdt_cfg -work_dir . -f prog.tcl

create: $(EDA)-create
synth: $(EDA)-synth
impl: $(EDA)-impl
prog: $(EDA)-prog

$(SYSTEM_RTL): cmake-build

run: $(SYSTEM_RTL)
	cd $(CMAKE_BINARY_DIR)
	$(CMAKE_BINARY_DIR)/$(SYSTEM_MODULE)

switch-clion:
	-rm -rf .idea-idea
	-mv .idea .idea-idea
	-mv .idea-clion .idea

switch-idea:
	-rm -rf .idea-clion
	-mv .idea .idea-clion
	-mv .idea-idea .idea

just-run: cmake-just-build
	cd $(CMAKE_BINARY_DIR) && $(CMAKE_BINARY_DIR)/$(SYSTEM_MODULE)

clean-config:
	-rm -rf .config .console.config .config.old

clean-espresso:
	-rm -rf espresso*

clean-download:
	-rm -rf download/

clean-installs:
	-rm -rf src/test/resources/bin/ src/test/resources/lib/
	-rm -rf src/test/resources/*.x src/test/resources/*.a src/test/resources/*.so src/test/resources/*.dll

clean: clean-config clean-installs clean-espresso
	-rm -rf out/ test_run_dir/ *.swp *build*/ project/ target/ .bsp/ espresso* .bloop/ .metals/ *.log


.PHONY: nemu generate ci idea ci-test all-test \
	clean clean-config \
	test sbt-test mill-test clean-link clean-installs clean-espresso clean-download \
	build cmake-build default-test \
	synth impl prog \
	vivado-synth vivado-create vivado-impl \
	pds-synth pds-create pds-impl pds-prog \
	verilog sbt-file generate-config \
	run just-run cmake-just-build
