include(FetchContent)

set(RISCV_NAME riscv)
#set(RISCV_URL https://mirror.msys2.org/mingw/mingw64/mingw-w64-x86_64-riscv64-unknown-elf-gcc-10.1.0-2-any.pkg.tar.zst)
set(RISCV_URL https://mirrors.tuna.tsinghua.edu.cn/msys2/mingw/mingw64/mingw-w64-x86_64-riscv64-unknown-elf-gcc-10.1.0-2-any.pkg.tar.zst)

include(FetchContent)
FetchContent_Declare(${RISCV_NAME}
        URL ${RISCV_URL}
        SOURCE_DIR ${CMAKE_SOURCE_DIR}/download/${RISCV_NAME}
        URL_HASH SHA1=F8B2CF3427A4EED054CCB7A0B9ADE2069640E1B6)
FetchContent_GetProperties(${RISCV_NAME})
if (NOT ${RISCV_NAME}_POPULATED)
    message(STATUS "Downloading ${RISCV_NAME} from ${RISCV_URL}")
    FetchContent_Populate(${RISCV_NAME})
endif ()

# set(RISCV_ROOT ${FETCHCONTENT_BASE_DIR}/${RISCV_NAME}-src/mingw64)
