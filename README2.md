# Chisel-X

[toc]

## 概述

将 Chisel 加入 FPGA 硬件设计流程，提供模块测试、硬件仿真、系统测试、差分测试等功能。

## 设计思路

当前，想要将 Chisel 语言加入 Vivado 工作流程，需要先根据 Chisel 生成 Verilog，再将 Verilog 封装为 IP 核或作为源码导入，才能和其他模块一同完成硬件设计。这其中有一些需要解决的问题：

1. 如果是使用 Chisel 开发计算机系统，需要提供便利的差分测试环境。
2. Chisel 综合出的 Verilog 中间代码可读性非常差，人类几乎无法理解。这会让后端工程师很困惑。另外 Chisel 代码的细微改动都会让生成的 Verilog 面目全非，后端工程师会更加难受。
3. 使用 Vivado IDE 开发的话总是动鼠标连线，开发不够自动化。
4. 需要适配线上的自动化测试环境。
5. 模块之间的连线没有 Vivado 的 Block Design 清晰，但是 Block Design 开发不够自动化。
6. 总线系统需要方便规划，如模块地址等。
7. 需要方便地生成标准化的 C/C++ BSP。
8. 如果脱离 Vivado IDE，还需要烧写工具。（其实运行 tcl 也可以。）
9. 因为脱离了 Zynq 系统，不能用 Vivado SDK，需要自己设计构建工具。
10. 和系统调试器 System ila 之间的适配问题。

一些想法：

0. 尽可能地做覆盖全面（时间和空间，aka 各个开发阶段和各个模块）的测试。即便是软件开发，Bug 的发现和修复的成本在越靠后的开发阶段越大。硬件开发修 Bug 的成本更大。应该坚持「早发现，早治疗」的原则。
1. 使用 Block Design
   1. 在调试的时候仍然可以使用 Block Design 管理模块和模块之间的联系。可以使用 Vivado 编辑 Block Design，方便接线管理，同时方便模块综合缓存。
   2. 在 Block Design 下的模块将不会每次都由 Chisel 生成，以方便缓存。
   3. 需要提供 Block Design 和 Chisel 代码之间的转换方式，转换为 Chisel 的 top 模块再由这个 top 模块生成 Verilog，便于硬件仿真（Verilator、iverilog）。
   4. Block Design 的总线地址和内存分配也可以和 Chisel 同步。
   5. bd 文件和 Chisel 文件需要以文件更新时间最新为准。
2. 原 Xilinx IP 的使用
   1. FPGA 里集成有本质是 ASIC 的硬宏，比如 BRAM，DSP，PLL。因为 ASIC 和 LUT 有本质的不同，这些硬宏的性能（延迟，面积，功耗）远好于等价的 LUT 实现。在设计中能使用硬宏应尽量使用硬宏。比如乘法器或除法器，用 DSP 的实现远优于 LUT 实现。Xilinx IP 可以显示地为模块匹配相应的硬宏资源，而 Chisel 及其生成的 Verilog 中间代码无法显示地匹配硬宏资源。某些情况下综合器可以自行推断并匹配相应的硬宏（一般是 Chisel 的 SyncReadMem 生成 Verilog 的寄存器堆匹配 BRAM 硬宏），或者在 Verilog 代码中加入 progma 来指导综合器匹配。而对于除法器或者乘法器这样需要 DSP 的模块，综合器无法为其匹配相应的硬宏，最好直接在综合中使用 Xilinx IP 来提高性能和节省资源。
   2. 如 AXI Connecter、BRAM Generator 等。
   3. AXI 系列 IPs 需要解决：
      1. AXI 总线接口的 Chisel 实现
      2. AXI 接口的调试：仿真过程中调试和硬件实际调试（不建议）
   4. BRAM Generator 等：需要综合为使用 BRAM 资源的电路。
   5. 可以尝试将 Xilinx IP 在 Chisel 中用黑盒替代。在仿真时使用等价的 Chisel 模块，而综合时使用 Xilinx IP。
3. 仿真
   1. iverilog：用于仿真小规模电路，解释运行，大电路仿真慢。
   2. Verilator：用于仿真较大规模电路。Verilog 会转化成等价的 C 代码编译执行，效率远高于解释执行。适合仿真 CPU 等大电路系统。
   3. Chisel 软件仿真：可以仿真 CPU 的运行等，基于 Scala 编译到 JVM 运行，适合仿真小系统和进行差分测试。直接使用 Chisel，所以不能有除了 Chisel 之外的其他模块。Chisel 的 tester 仿真粒度为 ** 网表 **，而且 FIRRTL 团队现阶段没有优化计划，所以 Chisel Tester 的仿真非常非常慢。
4. 差分测试
   1. 参考实现（REF）
      1. TDT4255 的 Scala VM：基于 TDT4255 的 RISCV 五级流水线 CPU 课程代码，提供面向 Chisel 的参考。性能很差，仅适合单条指令的正确性测试和小的程序片段。但是运行很方便，写完 Chisel RTL 一键测试马上就可以得到反馈。
      2. NEMU：基于南大 ics-pa 课程代码，提供面向系统和外设的逐指令的参考。另一方面 NEMU 的仿真粒度是指令，可以方便地加入各种 TRACE 工具，便于量化分析性能瓶颈。
      3. QEMU、Spike：提供系统和外设的参考。
      4. Zynq 的 AXI 总线：提供总线的参考。
   2. 测试对象（DUT）
      1. NEMU
      2. Chisel 模型
         1. CPU
         2. TLB
         3. Cache L1，L2
         4. AXI 总线
      3. Chisel 生成的 RTL
      4. 真机测试
   3. 可用的差分测试 (DUT<==>REF)
      - [x] NEMU <==> QEMU
      - [x] NEMU <==> Spike
      - [ ] Chisel <==> Scala VM
        - [x] CPU
        - [ ] TLB
        - [ ] AXI
        - [ ] Cache……
      - [ ] Chisel RTL <==> Verilator
        - [x] Chisel RTL <==> Software Model
      - [ ] Chisel <==> Zynq
        - [ ] Chisel AXI <==> System ila
        - [ ] Chisel CPU AXI <==> Zynq CPU AXI
        - [ ] Chisel CPU <==> NEMU on Zynq
      - [ ] Chisel <==> NEMU
   4. 设计流程
      1. 说明：
         1. Chisel RTL: C/C++ simulation with Verilator
         2. Software Model：使用 C++，仿照 Verilator 生成的文件，用软件模拟代替 Verilator 生成的电路模拟
         3. Chisel Functional：剥离接口后的外设模块内部功能
      2. 对 CPU 核心模块：
         1. Chisel <==> Scala VM
         2. Chisel Core (using Software Model) <==> NEMU (using Software Model)
         3. Chisel Core (using Chisel RTL) <==> NEMU (using Software Model)
         4. Chisel Core (using Chisel RTL) <==> NEMU (using Chisel RTL)
      3. 对无系统的 RTL 电路：
         1. Chisel <==> Chisel IO Tester (Simple)
         2. Chisel RTL <==> Software Model
      4. 对外设：需要 Verilator 支持 AXI 接口；需要保证 ChiselAXI 正确
         1. Chisel Functional <==> Software Mode
         2. Chisel <==> Chisel IO Tester (Simple)
         3. Chisel RTL (with ChiselAXI) <==> NEMU (using Software Model，MMIO)
      5. RTL 设计验证完成，交给 Vivado（TCL 命令行）处理。
5. 储存
   1. 寄存器文件：使用 BRAM
      1. 使用立即读取功能，即不等待一周期读取而直接读取
      2. 而后可以与 Zynq 联调
   2. 寄存器文件：使用 CPU 内部寄存器
      1. 向外引出读取接口
   3. 内存：使用 BRAM
      1. 性能优秀，单周期读取，当前周期写入
      2. 容量不足，只适合调试使用
   4. 内存：使用 DDR3
      1. 在 Zynq 开发板上不能直接联系到 DDR，需要经过 Zynq 核
      2. 可能可以在 Zynq 上进行调试，Chisel RTL <==> NEMU on Zynq，不断比较内存，Chisel Core 通过 Zynq 读写 DDR3
      3. 需要 Chisel 实现读写 DDR3 时序
6. BSP：Board Support Package
   1. 针对 CPU 内部设备和外部设备 *生成* 对应的 C/C++ 调用代码。
      1. 使用 Kconfig 进行配置，生成公共头文件
         1. 头文件放在 `${CMAKE_BINARY_DIR}/include/generated/autoconf.h`
      2. Python: `kconfiglib`
   2. 基于 AXI 总线通讯：AXI4/AXI4Lite
      1. 仅使用 AXI4Lite？
      2. 仿真侧使用 MMIO 直接模拟
   3. 模块代码结构：
      1. BSP 代码、RTL 代码、Software Model 放在 `main/scala/modules/${module-name}/`
      2. Chisel Verilog Generator、Chisel IO Tester 部分放在 `test/scala/modules/${module-name}/`

### 总线系统

计划使用基于 `Decoupled` 的 `ready-valid` 片内访存总线 `ChiBus`。结构大致为：

```mermaid
flowchart LR
	subgraph Bus
	BusController(总线控制器)
	DDRController[DDR控制器]
	DDR(DDR3内存)
	Uart[串口]
	Vga[VGA视频输出]
	DDRController <--> DDR
	BusController <--> Uart & Vga & DDRController
	end

	subgraph HHVM
	core(CPU核心)
	I$[(ICache)]
	D$[(DCache)]
	L2$[(L2Cache)]
    MemoryController(内存控制器)
	core <--"Naive"--> I$ & D$ <--"ChiBus"--> L2$ <--"ChiBus"--> MemoryController <--"AXI"--> BusController
	D$ <--"MMIO"--> MemoryController
	end
```

1. `Cache`模块：Cache 内容储存

   ![2wayCache](README.assets/20210107112341461.jpg)

   1. 接口：`CacheIO`

      ```mermaid
      flowchart LR
      	subgraph Master
      		MwriteEnable(writeEnable)
      		Maddress(address)
      		MreadCacheLine(readCacheLine)
      		MwriteCacheLine(writeCacheLine)
      	end
      	subgraph Slave
      		SwriteEnable(writeEnable)
      		Saddress(address)
      		SreadCacheLine(readCacheLine)
      		SwriteCacheLine(writeCacheLine)
      	end
      	
      	MwriteEnable --> SwriteEnable
      	Maddress --> Saddress
      	SreadCacheLine --> MreadCacheLine
      	MwriteCacheLine --> SwriteCacheLine
      ```

      

## 开发记录

### 目录结构

```
├─.github
│  └─workflows		# CI files
├─.idea				# IntelliJ Project files
├─build				# Default CMake build dir
├─ChiselX			# ScalaTest: ChiselX
│  └─src
│      ├─main
│      │  └─scala
│      │      └─modules			# RTL Modules, including...
│      │          ├─${module-name}
│      │          │  ├─bsp		# Board Support Package
│      │          │  ├─main		# RTL
│      │          │  └─model	# Software model
│      │      └─utils			# Tools
│      └─test
│          ├─java				# Java source dir
│          ├─resources			# ScalaTest run resources
│          └─scala				# Scala source dir
│              ├─difftest		# For difftest: CPU Core <==> 
│              ├─lang			# Language test for building test
│              └─modules		# ScalaTest for RTL Modules
│                  └─${module-name}
├─cmake		# CMake scripts
├─nemu-lite	# => nemu-lite: A high-performance System Simulator
├─hhvm		# => hhvm: HIT Hyperthreading Vector Machine(Silly Lake v0)
├─scripts	# Makefile / Python scripts / Build tools
└──tests		# Test files for CTest
   ├─shared		# Test library
   └─${verilator-test-name}/*.v		# Verilator 测试用 RTL
```

```
${CMAKE_BINARY_DIR}/
│  .config.json					# generated from ${X_ROOT}/.config by config2json.py
│  								# Targets
│  Dummy.exe					# ${model-name}: Verilator without Software Model
│  ModelDummy.exe				# Model${model-name}: Verilator with Software Model
│  libjna-test.dll
│  libjna-test.dll.a
│  model-verilator-model-test.exe
│  verilator-io-test.exe
│  verilator-model-test.exe
│  verilator-test.exe
│  x-cpp-lang-test.exe
│  x-lang-test.exe
├─chisel-rtl					# Chisel RTL directory
│      ${module-name}.v			# each module has one .v file
├─chisel-tmp					# Chisel temp dir for verilog generation
│  └─${module-name}
├─include/generated/autoconf.h	# generated by kconfiglib: genconfig
├─nemu-lite						# built files for nemu-lite
├─${isa-name}					# cross compile target
├─verilator						# verilator temp dir
│  ├─${module-name}
│  │  ├─${module-name}
│  │  └─Model${module-name}
│  └─${verilator-test-name}
└─_deps							# dependences
```

## 使用文档

### 示例

#### 运行本地CI

```shell
make ci				# 自动用 CMake 构建所有 C/C++ 目标
make ci-test		# 测试所有 Scala Test
cd build && ctest	# 进行所有 CTest
```

#### 生成 Verilog

##### mill

```shell
millw -i _.run [module-names]
# examples:
millw -i _.run				# generate all modules
millw -i _.run AXI			# generate one module
millw -i _.run hhvm L2Cache	# generate two module
```

Will generate all modules by default. Can select module names in args list, and can select target dir using `-td <dir>`.

##### sbt

```shell
sbt "project ChiselX ; run [module-names]"
```

#### 进行所有 ScalaTest

##### mill

```shell
millw -i _.test
```

指定运行某 ScalaTest 请使用 `sbt` 或者 VsCode / IDEA。

##### sbt

```shell
sbt test
```

进行所有 ScalaTest 测试。

#### 运行某 ScalaTest

##### sbt

```shell
sbt "testOnly modules.AXI4.AXI4Test"
```



#### 生成 IDEA 配置文件

```shell
make idea			# 使用 mill 生成 idea 配置文件
# IDEA 建议使用 sbt 项目，mill 经常不太好使
```

#### CMake 工程构建

```shell
# Configure, all necessary dependences will be automatically downloaded and cached
cmake -B build -S . -G Ninja -D BATCH_MODE=ON
# Build, can build with threads
cmake --build build -j
# Install Binary files and libraries
cmake --install build --prefix build/install
```

##### Available command line args:

1. `BATCH_MODE`: Do not show GUI when building and testing, for example VGA SDL Window.
2. `CI_MODE`: Configure as testing on CI, will install all packages, enable `BATCH_MODE`, disable `CONFIG_ITRACE`

##### Tested Compiler

1. GCC
2. MinGW-w64

##### Dependences

1. `dlfcn-win32`
   1. dynamic linking library for MinGW-w64
   2. auto download from https://github.com/dlfcn-win32/dlfcn-win32
2. `llvm`
   1. used to disassemble instructions
   2. will not install automatically
   3. Installation
      1. *nix: install `llvm` package
      2. Windows: Download from releases: https://github.com/llvm/llvm-project/releases/tag/llvmorg-11.0.0
3. `python3`: used to execute scripts and generate config
   1. *nix: `python3`
   2. Windows: `python`
4. `kconfiglib`
   1. used to manage config of this project
   2. will auto install using pip: https://github.com/ulfalizer/Kconfiglib
5. `readline`: used on *nix only, manually run `sudo apt-get install libreadline-dev`
6. `SDL2`
   1. used to display as VGA device
   2. Windows: will auto download from https://www.libsdl.org/release/SDL2-devel-2.0.20-mingw.tar.gz
   3. *nix: manually run `sudo apt-get install libsdl2-dev`

#### Kconfig配置

```shell
# CMake configure 后会自动安装 python3 的 kconfiglib，包含 menuconfig, genconfig, defconfig 等
menuconfig
# 改变了的配置请刷新到 CMake 的配置中
cmake -B build -S . -G Ninja
```

#### 使用编辑器编辑工程

1. 使用 JetBrains IntelliJ
   1. 直接打开目录即可
   2. IDEA 使用 SBT 格式工程
      1. 等待工程加载完成
      2. `sbt test`进行所有 ScalaTest
      3. 运行`main`：使用`应用程序(Application)`指定主类为`main`
   3. CLion 使用 CMake 工程
      1. 等待 CMake 加载完成
         1. 将会下载依赖项目到工程的临时目录
         2. 如果`.config`不存在就生成默认配置
      2. 编译运行各个 targets
         1. 在 CLion 右上角选择构建目标
         2. 可以选择单个目标或者 CTest 或者 Makefile
      3. 运行 CTest
         1. 选择构建目标为 `所有 CTest`
         2. 运行构建目标
         3. 会多线程运行 CTest 项目
2. 使用 Visual Studio Code
   1. 直接打开目录即可
   2. CMake 项目使用 CMake 插件
   3. Scala 项目使用 Metals 插件

### 模块目录格式

```
├─ChiselX
│  └─src
│      ├─main
│      │  └─scala
│      │      └─modules			# RTL Modules, including...
│      │          ├─${module-name}
│      │          │  ├─bsp		# Board Support Package
│      │          │  ├─main		# RTL
│      │          │  └─model	# Software model
│      │      └─utils			# Tools
│      └─test
│          └─scala				# Scala source dir
│              └─modules		# ScalaTest for RTL Modules
│                  └─${module-name}
```

**模块列表&名称：** 由`/ChiselX/src/main/scala/modules/*`匹配的文件夹名称为准。建议模块采用大驼峰命名法（如 `MyTestModule`）。

### 模块代码结构

1. RTL

   1. 代码位于`ChiselX/src/main/scala/modules/${module-name}/main`下。
   2. `package modules.main.${module-name}`
   3. 可以`import modules.${other-module}.main._`来实现对其他模块的依赖。
   4. 顶层模块为`modules.${module-name}.main.${module-name}`，在`${module-name}.v`中为`${module-name}`。
   5. 生成的文件位于`/build/chisel-rtl/${module-name}.v`和`/build/chisel-tmp/${module-name}/*`下，其中构建文件夹因 CMake 指定位置不同可以不同，但是默认是`/build/`。
   6. CMake 会查看模块目录下所有 Scala 文件的 Hash 值，遇到文件改动的时候会通过 Millw 调用 Chisel 重新生成 Verilog 文件到 `${CMAKE_BINARY_DIR}/chisel-rtl/${module-name}.v`。

2. Scala

   1. `mill _.run`的参数为：`[Core Dummy2] [-td /home/chiro/chisel-x/build/chisel-rtl]`，方框参数为可选，`chisel-tmp`目录和`chisel-rtl`目录同级；如果没有指定模块列表就会生成所有模块。

   2. 在IDE或者编辑器中也可以指定运行`main`

   3. `GlobalConfig`数据在开始运行时加载，加载文件为：

      1. `/build/.config`：在`CI_MODE`的时候由 CMake 从`/.console.config`复制。
      2. 找不到 `1` 则使用`/.config`，一般在本地调试的时候会加载这个文件。

   4. `GlobalConfig.config`加载完成后可以直接这样用：

      ```scala
      object ConfigTester {
        import utils.GlobalConfigLoader.config
        println(f"X_DIFFTEST = ${config.X_DIFFTEST.asInstanceOf[Boolean]}")
      }
      ```

      即去除`CONFIG_`而且可以直接用成员运算符访问。

   5. `config.core`展示了如何方便地对储存的 config 进行数据类型转换。

   6. 建议使用`implicit`模块化使用参数，而不是引入全局参数。示例：

      ```scala
      import utils.GlobalConfigLoader.GlobalConfig
      class CPU(implicit c: GlobalConfig) {
          val iCache = new ICache
      }
      class ICache(implicit c: GlobalConfig)
      
      object generateVerilog extends App {
          import utils.GlobalConfigLoader.config
          val cpu = new CPU
      }
      ```

      `implicit`关键字的作用是自动在当前命名作用域内搜索此类型的参数，要求当前作用域下同类型的参数只有一个。

3. Software Model

   1. 因为 Verilator 已经使用了 `${module-name}.h`作为 Verilator 模型头文件，所以建议使用 `${module-name}.hpp`作为原模型的 Wrapper，放在`model/`或`model/include`下。

   2. `#include <${module-name}.hpp>`将会……

      ```c
      /**
       * 此文件能够依照宏配置决定当前模块的运行模式：
       * 1. RTL 模式，仅运行 Verilator
       * 2. Software Model 模式：仅运行软件模型
       * 3. Difftest 模式：RTL 和 Software Model 同时运行，并且进行差分测试
       */
      ```

   3. `${module-name}.hpp`在不同运行模式下对外部操作尽量是透明的，可以参考`Dummy2.hpp`。

   4. `run-modules`是一个特殊的 target，将可以 include 所有模块的`.hpp`文件，通过 C/C++ 完成模型之间的连接，以完成整个系统的系统模型构建。（未完成连接。）

4. Board Support Package（未完成）

   1. 模块目录下的`bsp/`下的代码是在当前配置下，当前模块在系统中的调用代码。
   2. 将会放 uboot / 自己写的驱动程序等。

### 模块与模块之间的关系

1. 模块与模块之间可以存在依赖关系。
   1. RTL：Chisel 代码中可以直接`import`其他模块的代码，最后综合出来的`${module-name}.v`就会包含其他模块。
   2. Software Model：在 Kconfig 中可以配置`XM_${MODULE_NAME}_DEPS`，添加对其他模块的软件模型（含RTL、Software Model）的依赖。
2. 各个模块可以互相独立进行测试。
   1. `model/${module-name}.cpp`为各个模块的单元测试的入口。

### 编译目标测试程序

打开`menconfig`中的`Nemu Lite config --> Test images config --> [ ] Enbale CPU test images compile`即可在 CMake 的 Confuigure 阶段编译目标平台测试程序。

目标平台测试程序放在`nemu-lite/tests/image/{c,asm}/**/*.{asm,S,s,c,cpp}`中。

### 测试命令行样例

```shell
# 本地写代码常用，加载/更新 `.config` 作为配置文件，配置后编译然后将编译的安装文件复制给 Scala 调用
make
# 使用 mill 进行所有 Scala Test 测试
make mill-test
# 使用 sbt 进行所有 Scala Test 测试
make sbt-test
# 使用默认测试方式运行 ScalaTest
make test
```

```shell
# 删除除了 `download/` 的所有生成文件，包括生成的配置文件
make clean
```

```shell
# 使用 IDEA 以 Mill 工程的方式打开文件之前，需要用 mill 生成 IDEA 的工程文件
make idea
```

```shell
# 在本地测试在 CI 上的运行，即定义 `BATCH_MODE=ON` 和 `CI_MODE=ON`，配置后编译然后将编译的安装文件复制给 Scala 调用
make ci
# 测试 sbt 和 mill
make ci-test
```

```shell
# 已经配置了项目，只修改了 C/C++ 代码的情况下不重新配置，直接编译
# 修改 nemu-lite 和本项目 C/C++ 部分的时候用到
make build
```

```shell
# 运行 CTest
make ctest
```

```shell
# 生成模块对应电路 Verilog (自动添加 NegativeResetWrapper)
make verilog
```

```shell
# 对模块生成 Vivado 工程 (覆盖原有)
make verilog-create MODULES=AXI4
# 指定所有可以生成的模块生成 Vivado 工程
make verilog-create
```

```shell
# 对模块生成 Vivado 工程 (覆盖原有) 并综合，然后给出时序报告
make verilog-synth MODULES=AXI4
# 指定所有可以生成的模块生成 Vivado 工程并同时综合，然后给出时序报告
make verilog-synth
```

```shell
# 对模块生成 Vivado 工程 (覆盖原有) 并实现，然后给出时序报告
make verilog-impl MODULES=AXI4
# 指定所有可以生成的模块生成 Vivado 工程并同时实现，然后给出时序报告
make verilog-impl
```

```shell
# re-generate .config, .console.config and head file from Kconfig
make generate-config
```

```shell
# make all and run System
make run
```

### Notes

1. 当在 Linux 平台使用 `Address Sanitizer` 的时候，如需要手动运行程序，请先：
   ```shell
   export LD_PRELOAD=`find /usr/lib/gcc -name "libasan.so"`
   ```
   但是不要加入 `.bashrc` 等，不然会检测其他程序的内存泄露。