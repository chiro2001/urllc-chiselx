// See README.md for license details.

ThisBuild / scalaVersion     := "2.13.8"
ThisBuild / version          := "0.3.0"
ThisBuild / organization     := "com.github.crarch"

val chiselVersion = "3.5.3"
val chiselTestVersion = "0.5.3"

lazy val root = (project in file("."))
  .settings(
    name := "ChiselX",
    Test / parallelExecution := false,
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.2.12",
      "org.scalactic" %% "scalactic" % "3.2.12",
      "edu.berkeley.cs" %% "chisel3" % chiselVersion,
      "edu.berkeley.cs" %% "chiseltest" % chiselTestVersion % "test",
      "net.java.dev.jna" % "jna" % "5.11.0",
      // for logger
      "com.lihaoyi" %% "sourcecode" % "0.2.8",
      // for color print
      "com.lihaoyi" %% "fansi" % "0.3.1",
      // for progress bar
      "com.github.nscala-time" %% "nscala-time" % "2.30.0",
      "jline" % "jline" % "2.14.6"
    ),
    scalacOptions ++= Seq(
      "-language:reflectiveCalls",
      "-deprecation",
      "-feature",
      "-Xcheckinit",
      // Scala 动态类
      "-language:dynamics",
      "-P:chiselplugin:genBundleElements"
    ),
    addCompilerPlugin("edu.berkeley.cs" % "chisel3-plugin" % chiselVersion cross CrossVersion.full)
  )
