set top %top%
set generate_timng_report %generate_timng_report%

set project_name $top

set top_file ../../build/chisel-rtl/${top}Wrapper.v
set top_sim_file ../../scripts/sim/${top}Testbench.v

# define FPGA Chip
# set device PGL22G-6MBG324
set family    Logos
set device    PGL22G
set package   MBG324
set speed     -6
set_arch      -family $family -device $device -speedgrade $speed -package $package

set jobs 20

# define the output directory area
set   project_dir [file normalize .]
