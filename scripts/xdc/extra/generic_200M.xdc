# clock and reset
set_property -dict {PACKAGE_PIN AC19 IOSTANDARD LVCMOS33} [get_ports clock]
set_property -dict {PACKAGE_PIN Y3 IOSTANDARD LVCMOS33} [get_ports resetN]

set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets clock]

create_clock -period 5.000 -name clk_200M [get_ports clock]

# misc
set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 50 [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
