`timescale 10ns/10ns

module design_zynq_testbench();
    reg [7:0] io_adcSource = 0;
    wire [7:0] io_dacOut;
    wire io_adcClock;
    wire io_dacClock;

    initial begin
        #9600
        #9600 io_adcSource <= 32'h55;
        #9600 io_adcSource <= 32'h02;
        #9600 io_adcSource <= 32'h04;
        #9600 io_adcSource <= 32'h12;
        #9600 io_adcSource <= 32'h36;
        #9600 $finish;
    end

    design_zynq u_design_zynq(
        .io_adcSource(io_adcSource),
        .io_dacOut(io_dacOut),
        .io_adcClock(io_adcClock),
        .io_dacClock(io_dacClock)
    );

endmodule