module design_tester_testbench();
    reg clock = 0;
    reg resetN = 0;

    always #1 clock <= ~clock;

    reg [7:0] io_adcSource = 0;
    wire [7:0] io_dacOut;
    wire io_adcClock_sender;
    wire io_dacClock_sender;
    wire io_adcClock_receiver;
    wire io_dacClock_receiver;

    initial begin
        #0 io_adcSource <= 32'h55;
        #6 resetN <= 1;
        #9600 io_adcSource <= 32'h02;
        #9600 io_adcSource <= 32'h04;
        #9600 io_adcSource <= 32'h12;
        #9600 io_adcSource <= 32'h36;
        #9600 $finish;
    end

    design_tester u_design_tester(
        .clock(clock),
        .resetN(resetN),
        .io_adcSource(io_adcSource),
        .io_dacOut(io_dacOut),
        .io_adcClock_sender(io_adcClock_sender),
        .io_dacClock_sender(io_dacClock_sender),
        .io_adcClock_receiver(io_adcClock_receiver),
        .io_dacClock_receiver(io_dacClock_receiver)
    );

endmodule