`timescale 10ns / 1ns

module Dummy2Testbench();

reg clock = 0;
reg resetN = 0;
reg [7:0] a = 'h55;
reg [7:0] b = 'haa;
wire [7:0] c;
wire [7:0] d;

always #1 clock <= ~clock;

initial begin
  #6 resetN <= 1;
  #1000 $finish;
end

Dummy2 u (
  .clock(clock),
  .resetN(resetN),
  .io_in_a(a),
  .io_in_b(b),
  .io_out_c(c),
  .io_out_d(d)
);

endmodule
