module mul_test(
  input clock, 
  input resetN, 
  input [31:0] A,
  input [31:0] B,
  output [63:0] P
);
reg [31:0] Areg;
reg [31:0] Breg;
reg [63:0] Preg;
wire [63:0] Pwire;
assign P = Preg;
always @(posedge clock) begin
  Areg <= A;
  Breg <= B;
  Preg <= Pwire;
end
mult_gen_0 mul(
  .A(Areg), .B(Breg), .P(Pwire), .CE(1), .CLK(clock)
);
endmodule