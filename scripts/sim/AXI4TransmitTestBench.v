`timescale 10ns / 1ns

module AXI4TransmitTestbench();

reg clock = 0;
reg resetN = 0;
wire m_valid;
wire s_valid;
wire [31:0] m_data;
wire [31:0] s_data;
wire [3:0] m_state;
wire [3:0] s_state;

always #1 clock <= ~clock;

initial begin
  #600 resetN <= 1;
  #6000 $finish;
end

design_1_wrapper u (
  .clk_100MHz(clock),
  .reset_rtl_0_0(resetN),
  .m_valid(m_valid),
  .m_data(m_data),
  .m_state(m_state),
  .s_valid(s_valid),
  .s_data(s_data),
  .s_state(s_state)
);

endmodule
