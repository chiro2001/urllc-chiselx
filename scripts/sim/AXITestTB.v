`timescale 10ns / 1ns
module axi_test_tb();
reg clock = 0;
reg resetN = 0;
always #1 clock <= ~clock;
initial begin
  #4 resetN <= 1;
end
axi_test_wrapper u_axi_test_wrapper(
  .clock(clock), .resetN(resetN)
);
endmodule
