import re
with open("pf.log", "r") as f:
  lines = f.readlines()
  data_lines = [l.replace("\n", "").split()[-9:] for l in lines if 'pf[' in l]
  [list.reverse(l) for l in data_lines]
  print(data_lines[0])
  data_lines = [''.join(l) for l in data_lines]
  for i in range(len(data_lines)):
    data_lines[i] = [data_lines[i][j:j+2] for j in range(0, len(data_lines[i]), 2)]
    data_lines[i] = ['0x' + l for l in data_lines[i]]
    list.reverse(data_lines[i])
  print(f"{len(data_lines[0])} x {len(data_lines)}")
  # code_lines = ["{ " + ','.join(l) + " }" for l in data_lines]
  code_lines = [','.join(l) for l in data_lines]
  # code = f'static const unsigned char pf_code[{len(data_lines)}][{len(data_lines[0])}] __attribute__((section(".pf_code"))) ' + """{
  code = f'static const unsigned char pf_code[{len(data_lines) * len(data_lines[0])}] __attribute__((section(".pf_code"))) = ' + """{
  """ + ",".join(code_lines) + """
};"""
  # with open("pf_code.c", "w") as f2:
  #   f2.write(code)
  data = []
  print(data_lines[0])
  for l in data_lines:
    d = [int(i, 16).to_bytes(length=1, byteorder='little') for i in l]
    data.extend(d)
  with open("pf_code_32M.bin", "wb") as f3:
    [f3.write(d) for d in data]
  