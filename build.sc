// import Mill dependency

import mill._
import mill.scalalib.TestModule.ScalaTest
import mill.scalalib._
// support BSP
import mill.bsp._

trait MillSettings extends SbtModule {
  val scalaVersionMajor = "2"
  val scalaVersionMinor = "13"
  val scalaVersionRevision = "8"

  override def scalaVersion = s"$scalaVersionMajor.$scalaVersionMinor.$scalaVersionRevision"

  override def scalacOptions = Seq(
    "-language:reflectiveCalls",
    "-deprecation",
    "-feature",
    "-Xcheckinit",
    "-P:chiselplugin:genBundleElements"
  )

  override def ivyDeps = Agg(
    ivy"org.scalatest::scalatest:3.2.11"
  )
}

object root extends MillSettings {
  override def millSourcePath = os.pwd
  override def scalacOptions = super.scalacOptions() ++ Seq(
    // Chiro: fix `value xxx is not a member of chisel3.Bundle`
    "-Xsource:2.13",
    // Scala 动态类
    "-language:dynamics",
    "-unchecked",
    "-deprecation"
  )

  override def ivyDeps = super.ivyDeps() ++ Agg(
    ivy"edu.berkeley.cs::chisel3:3.5.2",
    ivy"net.java.dev.jna:jna:5.11.0",
    ivy"edu.berkeley.cs::chiseltest:0.5.2",
    ivy"org.scalactic::scalactic:3.2.12",
    // expert println debugging
    ivy"com.lihaoyi::sourcecode:0.2.8",
    // for color print
    ivy"com.lihaoyi::fansi:0.3.1",
    ivy"com.github.nscala-time::nscala-time::2.30.0",
    ivy"jline::jline::2.14.6"
  )

  override def scalacPluginIvyDeps = super.scalacPluginIvyDeps() ++ Agg(
    ivy"edu.berkeley.cs:::chisel3-plugin:3.5.1"
  )

  object test extends Tests with ScalaTest {
    println("ChiselX Project Test with ScalaTest & ChiselTest")
  }

  object run extends App {
    println("ChiselX run")
  }
}
