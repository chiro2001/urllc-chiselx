// Test verilator io
module verilator_io_test (
    input wire clk,
    input wire resetn,
    input wire a,
    input wire b,
    output wire c
);
    initial begin
        $display("IO test!");
    end
    reg c_reg;
    assign c = c_reg;

    reg [7:0] cnt;
    parameter CLK_MAX = 8'd10;

    always @ (posedge clk or negedge resetn) begin
        if (~resetn) begin
            c_reg <= 1'b0;
            cnt <= 8'b0;
        end
        c_reg <= (a ^ b);
        $display("\tIO: a = %b, b = %b, c = %b, cnt = %03d", a, b, c, cnt);
        if (cnt == (CLK_MAX - 8'b1)) begin
            cnt <= 8'b0;
            $finish;
        end
        else begin
            cnt <= cnt + 8'b1;
        end
    end
endmodule
