//
// Created by Chiro on 2022/2/22.
//

#include <common.h>
#ifdef CONFIG_XM_CORE
#include <CoreModel.hpp>
#endif

int main() {
  puts("run modules main!");
  MUXDEF(CONFIG_XM_CORE, 
    printf("Yes, I have %s now!!!\n", typeid(Core).name()),
    printf("No core detected...\n")
  );
  return 0;
}