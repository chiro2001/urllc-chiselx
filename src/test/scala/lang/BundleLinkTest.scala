package lang

import chisel3._
import chiseltest._
import org.scalatest.flatspec._
import org.scalatest.matchers._

class BundleLink extends Module {
  class Link(modeOut: Boolean) extends Module {
    val io = IO(new Bundle {
      val pin1 = if (modeOut) Output(Bool()) else Input(Bool())
      val pin2 = if (modeOut) Output(Bool()) else Input(Bool())
    })
    if (modeOut) {
      io.pin1 := true.B
      io.pin2 := false.B
    }
  }
  class LinkDiff(modeOut: Boolean) extends Module {
    val io = IO(new Bundle {
      val pin1 = if (modeOut) Output(Bool()) else Input(Bool())
      val pin2 = if (modeOut) Input(Bool()) else Output(Bool())
    })
    if (modeOut) {
      io.pin1 := true.B
    } else {
      // io.pin2 := false.B
      io.pin2 := io.pin1
    }
  }
  // class Link1 extends Link(true)
  // class Link2 extends Link(false)
  class Link1 extends LinkDiff(true)
  class Link2 extends LinkDiff(false)
  // class Link1 extends Module {
  //   val io = IO(new Bundle {
  //     val pin1 = Output(Bool())
  //     val pin2 = Output(Bool())
  //   })
  //   io.pin1 := true.B
  //   io.pin2 := true.B
  // }
  // class Link2 extends Module {
  //   val io = IO(new Bundle {
  //     val pin1 = Input(Bool())
  //     val pin2 = Output(Bool())
  //   })
  //   io.pin2 := false.B
  // }
  val link1 = Module(new Link1)
  val link2 = Module(new Link2)
  link1.io <> link2.io
  // link2.io <> link1.io
  // link1.io.pin1 := link2.io.pin1
  // link2.io.pin2 := link1.io.pin2
  val io = IO(new Bundle {
    val pin1 = Output(Bool())
    val pin2 = Output(Bool())
  })
  io.pin1 := link1.io.pin1
  io.pin2 := link2.io.pin2
  // link1.io.pin2 := io.pin1
  // io <> link1.io
}

class BundleLinkTest extends AnyFlatSpec with ChiselScalatestTester with should.Matchers {
  behavior of "Bundle Link Test"
  it should "pass the test" in {
    test(new BundleLink) { c =>
      c.io.pin1.expect(true.B)
      c.io.pin2.expect(true.B)
    }
  }
}
