package lang

import chisel3._
import chiseltest._
import org.scalatest.flatspec._
import org.scalatest.matchers._

class UIntToBundle extends Module {
  class TestBundle extends Bundle {
    val data1 = UInt(4.W)
    val data2 = UInt(8.W)
  }
  val io = IO(new Bundle {
    val button = Input(Bool())
    val inputUInt = Input(UInt(12.W))
    val outputUInt = Output(new TestBundle)
  })
  // io.outputUInt := Mux(io.button, io.inputUInt.asTypeOf(new TestBundle), 0.U.asTypeOf(new TestBundle)).asUInt
  io.outputUInt := Mux(io.button, io.inputUInt.asTypeOf(new TestBundle), 0.U.asTypeOf(new TestBundle))
}

class UIntToBundleTest extends AnyFlatSpec with ChiselScalatestTester with should.Matchers {
  behavior of "Bundle convert Test"
  it should "pass the test" in {
    test(new UIntToBundle) { c =>
      c.io.button.poke(false.B)
      c.io.inputUInt.poke(0x55.U)
      c.io.outputUInt.data1.expect(0.U)
      c.io.outputUInt.data2.expect(0.U)
      c.io.button.poke(true.B)
      c.io.inputUInt.poke(0xabb.U)
      c.io.outputUInt.data1.expect(0xa.U)
      c.io.outputUInt.data2.expect(0xbb.U)
    }
  }
}
