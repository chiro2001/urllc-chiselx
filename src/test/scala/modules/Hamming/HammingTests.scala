package modules.Hamming

import Chisel.log2Ceil
import chisel3._
import chisel3.stage.PrintFullStackTraceAnnotation
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

import scala.collection.mutable.ArrayBuffer

class HammingTestModule extends Module {
  val io = IO(new Bundle {
    val source = Input(UInt(64.W))
    val dest = Output(UInt(64.W))
    val encoded = Output(UInt(72.W))
    val err = Input(Bool())
    val errIndex = Input(UInt(7.W))
    val err2 = Input(Bool())
    val errIndex2 = Input(UInt(7.W))
    val fix = Output(Bool())
  })
  val encoder = Module(new HammingEncode)
  val decoder = Module(new HammingDecode)
  encoder.io.data := io.source
  io.dest := decoder.io.data
  io.fix := decoder.io.fixed
  val encoderOut = VecInit(encoder.io.hamming.asBools)
  io.encoded := encoder.io.hamming
  val encoded = WireInit(encoderOut)
  when(io.err) {
    encoded(io.errIndex) := !encoderOut(io.errIndex)
  }
  when(io.err2) {
    encoded(io.errIndex2) := !encoderOut(io.errIndex2)
  }
  decoder.io.hamming := encoded.asTypeOf(decoder.io.hamming)
}

class HammingTests extends AnyFlatSpec with ChiselScalatestTester with should.Matchers {
  behavior of "Hamming"
  it should "do static check" in {
    print("indexList: ")
    for (i <- Hamming.indexList.indices) print("%3d".format(i))
    print("\n           ")
    for (i <- Hamming.indexList) print("%3d".format(i))
    print("\nindexInv: ")
    for (i <- Hamming.indexInv.indices) print("%3d".format(i))
    print("\n          ")
    for (i <- Hamming.indexInv) print("%3d".format(i))
    println("")

     def testRound(src: Long): Unit = {
      val sourceData: ArrayBuffer[Int] = ArrayBuffer.from(Seq.fill(64)(0))
      var s = src
      for (i <- 0 until 64) {
        sourceData(i) = (s & 0x1L).toInt
        s = s >> 1
      }
      val sourceVec = Hamming.indexInv.map(i => if (i < 0) 0 else sourceData(i))
      val data = Hamming.indexInv.zipWithIndex.map {
        case (v, i) if v < 0 =>
          if (i == 0) {
            sourceVec.reduce(_ ^ _)
          } else {
            sourceVec.zipWithIndex
              .map(x => ((x._2 & (1 << log2Ceil(i))) != 0, x._1))
              .filter(_._1).map(_._2).reduce(_ ^ _)
          }
        case (v, _) => sourceData(v)
      }.toArray

      def printVec(vec: Seq[Int], name: String = ""): Unit = println(name + vec.map(_.toString).reverse.foldLeft("")(_ + _))

      printVec(sourceData.toSeq, "SrcData: ")
      printVec(sourceVec, "SrcVec:  ")
      printVec(data.toSeq, "Enc:     ")
      val destVec = Hamming.indexList.map(i => data(i))
      printVec(destVec, "Dst:     ")
      val checkBitsIndexes = Hamming.indexInv.zipWithIndex.filter(_._1 < 0).map(_._2)
      printVec(checkBitsIndexes, "Idx:     ")
      val checkBits = checkBitsIndexes.map(data(_))
      printVec(checkBits, "Chk:     ")

      for (errIndex <- 1 until 71) {
        data(errIndex) = data(errIndex) match {
          case 1 => 0
          case _ => 1
        }

        val checking = (0 until 8).map(n => if (n == 0) data.reduce(_ ^ _) ^ data.head else {
          data.zipWithIndex.filter(i => (i._2 & (1 << (n - 1))) != 0).map(_._1).reduce(_ ^ _)
        })
        printVec(checking, "Chking:  ")
        var checkedIndex = 0
        val isErr = checking.head != 0
        for ((v, i) <- checking.slice(1, 8).zipWithIndex) {
          checkedIndex = checkedIndex | (v << i)
        }
        require(errIndex == checkedIndex, s"errIndex($errIndex) != checkedIndex($checkedIndex)")
        require(isErr)
        data(checkedIndex) = data(checkedIndex) match {
          case 1 => 0
          case _ => 1
        }
      }
    }

    testRound(0x00aa12345678aaFFL)
  }
  it should "test correction" in {
    test(new HammingTestModule)
      .withAnnotations(Seq(PrintFullStackTraceAnnotation, WriteVcdAnnotation)) { d =>
        val value = 0x00aa12345678aaFFL
        d.io.source.poke(value)
        d.clock.step()
        d.io.encoded.expect("b101010100001001000110100010101010011110001010100101111111101111".asUInt)
        d.io.dest.expect(value)
        d.io.fix.expect(false)
        for (i <- 1 until 72) {
          d.io.err.poke(true)
          d.io.errIndex.poke(i)
          d.clock.step()
          d.io.dest.expect(value)
          d.io.fix.expect(true)
        }
        d.io.err.poke(true)
        d.io.errIndex.poke(63)
        d.io.err2.poke(true)
        d.io.errIndex2.poke(2)
        d.clock.step()
        d.io.fix.expect(false)
      }
  }
}