package modules.ASK

import chisel3._
import chisel3.stage.PrintFullStackTraceAnnotation
import chiseltest._
import modules.ASK.main.ASK
import modules.ASKReceier.main.ASKReceiver
import modules.ASKSender.main.ASKSender
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import utils.GlobalConfigLoader.config
import utils.generate

class ASKTests extends AnyFlatSpec with ChiselScalatestTester with should.Matchers {
  behavior of "ASKSender"
  it should "generate RTL" in {
    generate.generateVerilog(
      Map("ASKSender" -> (() => new ASKSender))
    )
  }

  it should "test send data" in {
    test(new ASKSender)
      .withAnnotations(Seq(PrintFullStackTraceAnnotation, WriteVcdAnnotation)) { d =>
        d.clock.setTimeout(0)
        d.io.start.get.poke(true.B)
        (0 until 10).foreach(i => {
          d.io.packSource.get.poke(i.U)
          d.clock.step(config.ask.sender.sourceBit)
        })
      }
  }

  behavior of "ASKReceiver"
  it should "generate RTL" in {
    generate.generateVerilog(
      Map("ASKReceiver" -> (() => new ASKReceiver))
    )
  }

  it should "test receive data" in {
    test(new ASKReceiver)
      .withAnnotations(Seq(PrintFullStackTraceAnnotation, WriteVcdAnnotation)) { d =>
        d.clock.setTimeout(0)
        d.clock.step(config.ask.clkPerBit * 3)
        // send (01)* for calibration
        for (_ <- 0 until 12) {
          d.io.adc.poke(config.ask.sender.value1)
          d.clock.step(config.ask.clkPerBit)
          d.io.adc.poke(config.ask.sender.value0)
          d.clock.step(config.ask.clkPerBit)
        }
        // send pre-code
        val preCode = config.ask.preCode.reverse
        for (c <- preCode) {
          if (c == '0') {
            println("write ", c)
            d.io.adc.poke(config.ask.sender.value0)
            d.clock.step(config.ask.clkPerBit)
          } else if (c == '1') {
            println("write ", c)
            d.io.adc.poke(config.ask.sender.value1)
            d.clock.step(config.ask.clkPerBit)
          }
        }
        // send length && hamming
        val partData = 0x550003
        for (i <- 0 until 24) {
          if (((partData >> i) & 0x1) == 1) {
            d.io.adc.poke(config.ask.sender.value1)
          } else {
            d.io.adc.poke(config.ask.sender.value0)
          }
          d.clock.step(config.ask.clkPerBit)
        }
        d.clock.step(config.ask.clkPerBit * 2)
      }
  }

  behavior of "ASK Connect"
  val maxBytes = 8
  it should "generate RTL" in {
    generate.generateVerilog(
      Map("ASK" -> (() => new ASK(maxDataBytes = maxBytes)))
    )
  }
  it should "test transmit data" in {
    val offset = config.ask.clkPerBit * 2 / 3
    test(new ASK(maxDataBytes = maxBytes))
      .withAnnotations(Seq(PrintFullStackTraceAnnotation, WriteVcdAnnotation)) { d =>
        d.io.clockOffset.poke(offset)
        d.clock.setTimeout(0)
        d.clock.step(config.ask.clkPerBit * 2)
        d.io.start.poke(false)
        // val testValue = 0x55
        // val testValue = 0x55
        val testValue = 0x00aa12345678aaFFL
        // val testValue = 0x0
        // val testValue = 0x1
        d.io.dataSource.poke(testValue)
        d.clock.step(config.ask.clkPerBit * 2)
        d.io.start.poke(true)
        d.clock.step(config.ask.clkPerBit * (8 + 8 + maxBytes * 8))
        d.clock.step(config.ask.clkPerBit * 1)
        d.io.dacOut.expect(testValue & 0xff)
      }
  }

  it should "test delay" in {
    test(new ASK(maxDataBytes = maxBytes)) { d =>
      d.io.clockOffset.poke(config.ask.clkPerBit * 2 / 3)
      d.clock.setTimeout(0)
      d.clock.step(config.ask.clkPerBit * 2)
      d.io.start.poke(false)
      val testValue: BigInt = 0x55aa1234
      d.io.dataSource.poke(testValue)
      d.clock.step(config.ask.clkPerBit * 2)
      d.io.start.poke(true)
      d.clock.step(config.ask.clkPerBit * (8 + 8 + maxBytes * 8))
      d.clock.step(config.ask.clkPerBit * 1)
      d.io.dacOut.expect(testValue & 0xff)
    }
  }

  it should "test delays" in {
    val total = 12
    val threads = (0 until total).map(i => {
      new Thread(() => {
        test(new ASK(maxDataBytes = maxBytes)) { d =>
          d.io.clockOffset.poke(config.ask.clkPerBit * i / total)
          d.clock.setTimeout(0)
          d.clock.step(config.ask.clkPerBit * 2)
          d.io.start.poke(false)
          val testValue: BigInt = 0x55aa1234 + i
          d.io.dataSource.poke(testValue)
          d.clock.step(config.ask.clkPerBit * 2)
          d.io.start.poke(true)
          d.clock.step(config.ask.clkPerBit * (8 + 8 + maxBytes * 8))
          d.clock.step(config.ask.clkPerBit * 1)
          d.io.dacOut.expect(testValue)
        }
      })
    })
    threads.foreach(t => t.start())
    threads.foreach(t => t.join())
  }
}
