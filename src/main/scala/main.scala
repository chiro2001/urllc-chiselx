import modules.Dummy2.main.Dummy2
import modules.ASK.main.ASK
import modules.ASKReceier.main.{ASKReceiver, Receiver}
import modules.ASKSender.main.{ASKSender, Sender}
import utils.GlobalConfigLoader.config
import utils.{BasicLogger, generate}
import utils.generate.generateModule

object main extends App {
  val generativeClasses = Map(
    "Dummy2" -> (() => new Dummy2),
    "ASK" -> (() => new ASK),
    "ASKSender" -> (() => new ASKSender),
    "Sender" -> (() => new Sender),
    "ASKReceiver" -> (() => new ASKReceiver),
    "Receiver" -> (() => new Receiver),
  )
  generate.generateVerilog(generativeClasses, args)
  BasicLogger.enableHardwareLogging = false
}
