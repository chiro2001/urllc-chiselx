package utils

import chisel3._
import utils.GlobalConfigLoader.GlobalConfig

trait BundleWithDefaultValue[T <: Data] {
  def getDefaultValue(implicit c: GlobalConfig): T
}
