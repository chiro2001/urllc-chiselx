package utils.module

import chisel3._

trait Bindable {
  def bindPort(port: Data): Unit
}
