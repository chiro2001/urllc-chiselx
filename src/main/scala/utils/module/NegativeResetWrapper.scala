package utils.module

import chisel3._

class NegativeResetWrapper
(module: => RawModule, ignoredPorts: Seq[String] = Seq("clock", "reset"), moduleName: String = null)
  extends BindableRawModule {
  val clock = IO(Input(Clock()))
  val resetN = IO(Input(Bool()))
  var gotModuleName: Option[String] = None
  withClockAndReset(clock, !resetN) {
    val inner = Module(module)
    gotModuleName = Some(inner.getClass.getName.split("\\.").last + "Wrapper")
    bindingModulePorts(inner, ignoredPorts = ignoredPorts)
  }

  override def desiredName =
    if (moduleName != null) moduleName
    else if (gotModuleName.nonEmpty) gotModuleName.get
    else super.desiredName
}
