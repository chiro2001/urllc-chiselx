package utils.module

import chisel3._
import utils.module.BindingModulePorts.bindModulePorts

class BindableModule extends Module with Bindable {
  override def bindPort(port: Data) = _bindIoInPlace(port)

  def bindingModulePorts(source: => RawModule, ignoredPorts: Seq[String] = Seq("clock", "reset")) =
    bindModulePorts(this, source, ignoredPorts = ignoredPorts)
}
