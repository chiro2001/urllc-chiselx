package utils

import chisel3._
import chisel3.util.experimental.BoringUtils


object Timer{
    class Logger(module_name:String){
        def apply(fmt:String,bits:Bits*)={
            val counter = Wire(UInt(32.W))
            counter := 0.U
            BoringUtils.addSink(counter, "counter")
            import BasicLogger.enableLogging
            import BasicLogger.enableHardwareLogging
            if (enableLogging && enableHardwareLogging) {
                printf("[cycle %d][" + module_name + "]", counter)
                printf(fmt, bits: _*)
                printf("\n")
            }
        }
    }
}


class Timer extends Module{
    val io=IO(new Bundle{
        // val counter=Output(UInt(64.W))
    })
    val counter=RegInit(UInt(32.W),0.U)
    counter:=counter+1.U
    BoringUtils.addSource(counter, "counter")
}
