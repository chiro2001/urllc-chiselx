package utils

class BasicLogger[T <: Any] {
  import BasicLogger._
  var logLevel = if (debugEnable) 0 else 1
  val levelTextWithColor = Seq(
    "DEBUG",
    "VERB",
    Console.GREEN + "INFO" + Console.RESET,
    Console.YELLOW + "WARN" + Console.RESET,
    Console.RED + "ERROR" + Console.RESET,
    Console.BLACK + Console.RED_B + "FATAL" + Console.RESET
  )
  val levelText = Seq(
    "DEBUG",
    "VERB",
    "INFO",
    "WARN",
    "ERROR",
    "FATAL"
  )

  protected def outputStringNewLine(str: String, xs: T*) = printf((if (str.endsWith("\n")) str.slice(0, str.length - 1) else str) + "\n", xs: _*)

  protected final def logIt(foo: String, level: Int, xs: T*)(implicit line: sourcecode.Line, file: sourcecode.File, name: sourcecode.Name) = {
    if (level >= logLevel && enableLogging) {
      outputStringNewLine((if (colorLog) fansi.Reversed.On("CHI").toString() else "") +
        f" ${if (colorLog) levelTextWithColor(level) else levelText(level)} [" +
        (if (filePathLog) f"${file.value}:${line.value} " else "") +
        f"${name.value}] $foo", xs: _*)
    }
  }

  def setLevel(level: Int) = logLevel = level

  def log(msg: => String, xs: T*)(implicit line: sourcecode.Line, file: sourcecode.File, name: sourcecode.Name): Unit = logIt(msg, LOG_LEVER_VERBOSE, xs: _*)

  def debug(msg: => String, xs: T*)(implicit line: sourcecode.Line, file: sourcecode.File, name: sourcecode.Name): Unit = logIt(msg, LOG_LEVER_DEBUG, xs: _*)

  def info(msg: => String, xs: T*)(implicit line: sourcecode.Line, file: sourcecode.File, name: sourcecode.Name): Unit = logIt(msg, LOG_LEVER_INFO, xs: _*)

  def warn(msg: => String, xs: T*)(implicit line: sourcecode.Line, file: sourcecode.File, name: sourcecode.Name): Unit = logIt(msg, LOG_LEVER_WARN, xs: _*)

  def error(msg: => String, xs: T*)(implicit line: sourcecode.Line, file: sourcecode.File, name: sourcecode.Name): Unit = logIt(msg, LOG_LEVER_ERROR, xs: _*)

  def fatal(msg: => String, xs: T*)(implicit line: sourcecode.Line, file: sourcecode.File, name: sourcecode.Name): Unit = logIt(msg, LOG_LEVER_FATAL, xs: _*)

  def printStackTrace(cls: Class[_]): Unit = {
    val elements = (new Throwable).getStackTrace
    info("Stack for " + cls.getName + ":")
    elements.foreach(element => {
      info(element.getClassName +
        "." + element.getMethodName +
        "(" + element.getFileName +
        ":" + element.getLineNumber +
        ")")
    })
  }
}

object BasicLogger {
  var enableLogging = true
  // var enableLogging = false
  // var enableHardwareLogging = true
  var enableHardwareLogging = false
  val debugEnable = false
  // val debugEnable = true
  val LOG_LEVER_DEBUG = 0
  val LOG_LEVER_VERBOSE = 1
  val LOG_LEVER_INFO = 2
  val LOG_LEVER_WARN = 3
  val LOG_LEVER_ERROR = 4
  val LOG_LEVER_FATAL = 5
  var colorLog = true
  var filePathLog = true
}