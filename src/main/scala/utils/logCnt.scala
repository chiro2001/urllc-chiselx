package utils

import chisel3._
import chisel3.util.experimental.BoringUtils
import utils.BasicLogger.enableHardwareLogging

object logCnt extends BasicLogger[Bits] {
  protected override def outputStringNewLine(str: String, xs: Bits*): Unit = {
    val counter = Wire(UInt(32.W))
    counter := 0.U
    val xs2 = Seq(counter) ++ xs
    BoringUtils.addSink(counter, "counter")
    if (enableHardwareLogging) {
      printf(
        "[cycle %d] " + (if (BasicLogger.colorLog) fansi.Reversed.On("HW").toString() else "HW") + " " + (if (str.endsWith("\n")) str.slice(0, str.length - 1) else str) + "\n",
        xs2: _*
      )
    }
  }
}
