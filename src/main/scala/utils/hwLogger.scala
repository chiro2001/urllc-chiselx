package utils

import chisel3._
import BasicLogger._

object hwLogger extends BasicLogger[Bits] {
  protected override def outputStringNewLine(str: String, xs: Bits*): Unit =
    if (enableHardwareLogging) printf(
      (if (BasicLogger.colorLog) fansi.Reversed.On("HW").toString() else "HW") + " " + (if (str.endsWith("\n")) str.slice(0, str.length - 1) else str) + "\n",
      xs: _*
    )
}
