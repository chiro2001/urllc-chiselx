package modules.Dummy2.main

import chisel3._
import utils.module.NegativeResetWrapper

class Dummy2(width: Int = 8) extends Module {
  val io = IO(Dummy2.getIOPort(width))
  val c = RegInit(0.U)
  io.out.c := c
  val xor = io.in.a ^ io.in.b
  io.out.d := xor
  c := xor
  val hole = Wire(new Bundle() {
    val foo = UInt(3.W)
    val ignored = UInt(2.W)
    // ignored := 0.U
    val bar = UInt(4.W)

  })
  hole.ignored := 0.U
  hole.foo := "b101".U
  hole.bar := "b010".U
  printf("%b\n", hole.asTypeOf(UInt(10.W)))
}

object Dummy2 {
  def getIOPort(width: Int) = new Bundle {
    val in = Input(new Bundle {
      val a = UInt(width.W)
      val b = UInt(width.W)
    })
    val out = Output(new Bundle {
      val c = UInt(width.W)
      val d = UInt(width.W)
    })
  }
}