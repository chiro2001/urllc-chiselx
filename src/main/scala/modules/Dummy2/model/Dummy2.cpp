//
// Created by Chiro on 2022/2/22.
//
#include <Dummy2.hpp>

int main(int argc, char **argv, char **env) {
  auto *top = new Dummy2;
  srand(time(nullptr));
  int cnt = 0;
  const int cnt_max = 10;
  auto exec = [&](int n) {
    top->clock = 0;
    top->eval();
    top->clock = 1;
    top->eval();
    cnt++;
  };
  top->reset = 1;
  exec(5);
  top->reset = 0;
  int a = 0, b = 0;
  while (!Verilated::gotFinish() && cnt < cnt_max) {
    top->io_in_a = a = rand() % 2;
    top->io_in_b = b = rand() % 2;
    // Evaluate model
    exec(1);
    printf("[%03lu] ", cnt);
    if (top->io_out_c != (a ^ b)) {
      printf("Err: a = %d, b = %d, c = %d\n", a, b, top->io_out_c);
    } else {
      printf("OK.\n");
    }
  }
  if (cnt == cnt_max) {
    Log("Warning: reach cnt MAX(%d)", cnt_max);
  }
  top->final();
  delete top;
  return 0;
}
