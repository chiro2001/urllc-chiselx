package modules.ASK.main

import chisel3._

object DataPackage {
  val maxDataBytes = 8

  assert(maxDataBytes == 8)

  def dataBits = maxDataBytes * 8

  def hammingDataBits = maxDataBytes * 8 + 8

  def packMaxBits = hammingDataBits

  def packBits = hammingDataBits.U

  def sourceDataDefault = VecInit(Seq.fill(8)(0.U(8.W)))
}