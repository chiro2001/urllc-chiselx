//
// Created by Chiro on 2022/2/22.
//

#include <model.h>

// FILE *log_fp = NULL;
// char *log_path = NULL;
// char *log_tail_path = NULL;
// uint64_t log_cnt = 0;

void init_log(const char *log_file) {}

bool VerilatedFake::begin = false;
bool VerilatedFake::finish = false;

bool VerilatedFake::gotFinish() { return finish; }

VerilatedFake::VerilatedFake() : clock(0), reset(1) {
  begin = true, finish = false;
}

void VerilatedFake::final() { finish = true; }

#ifdef USE_MODEL
void VerilatedFake::traceEverOn(bool enable) {
  IFNDEF(MODEL_ONLY, Verilated::traceEverOn(enable));
}
#endif