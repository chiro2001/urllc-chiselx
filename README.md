## 说明

1. 环境

   1. 推荐使用 Linux
   2. Windows 下应该也可以
   3. 环境变量
      1. 需要在命令行中设置 `http_proxy=<代理链接>`，如：
      2. Windows：`set http_proxy=http://r.chiro.work:14514`
      3. Linux：`export http_proxy=http://localhost:7890`
   4. 依赖等见下面执行部分

2. `make`：

   1. 自动下载并配置相关环境，需要访问网络
   2. 编译 Verilator
   3. 生成 Verilog

3. `sbt test`：运行所有测试

   1. 生成波形文件等位于 `test_run_dir` 
   2. 将会模拟发送端发送、接收端接收
   3. 并随机初始化中间延迟

4. `sbt run`：生成所有Verilog

5. `make vivado-create`

   1. 在 `vivado/` 下生成 Vivado 项目
   2. 如果需要对项目进行改动请将对应文件夹复制一份以防止被覆盖
   3. 需要 `vivado` 可执行文件在当前 `PATH` 中

6. `make vivado-imlp`：在 `vivado/` 下生成项目后进行综合、实现、生成比特流

7. `make <method> MODULES="aaa bbb"`：

   1. 指定生成 `aaa`、`bbb` 模块
   2. `<method>`：`create` / `synth` / `impl`

8. 可用的模块：

   1. `Dummy2`：示例模块
   2. `ASK`：
      1. 仅用于仿真
      2. 测试发送端 `ASKSender` 和接收端 `ASKReceiver` 的工作状态
      3. 输入数据源为直接的 8 bytes 而不是 8-bit ADC
   3. `ASKSender`：
      1. 同样仅用于仿真
      2. 输入数据是 64bit 所以需要再在外面包裹一层处理 ADC 输入
   4. `Sender`：
      1. 用于综合生成上板测试
      2. 输入数据是 8-bit ADC
   5. `ASKReceiver`：
      1. 用于仿真，输出是 64bit
   6. `Receiver`：
      1. 用于综合
      2. 输出数据是 8-bit DAC

9. `vivado-project-clocked`：调整时钟后的 Vivado 项目

10. 关于 Vivado 版本

    1. 使用 `make vivado-*` 生成的 Vivado 项目适用于所有版本的 Vivado
    2. 推荐使用 2018.1 以及以上

11. 关于时钟

    1. 默认使用板上晶振时钟（应该是 50MHz 或者 100MHz）

    2. 由于使用 ASK，所以不需要适应中频

    3. ```
                 Slower Clock
       ─────────┐           ┌─────────────┐             ┌─────────────
                │           │             │             │
                │           │             │             │
                │                         │
                │           ▲             │             ▲
                │           │             │             │
                │           │             │             │
                │           │             │             │
                │           │             │             │
                │           │             │             │
                └───────────┘             └─────────────┘
                             0             k/2          k
                 Source wave
                ┌─────────────────────────┐                          ┌──────────
                │                         │                          │
                │                         │                          │
                │                         │                          │
                │                         │                          │
                │                         │                          │
       ─────────┘                         └──────────────────────────┘
       ```

    4. 关于接收端的 DAC 时钟

       1. 默认为不分频，`dacClkDiv == 0`
       2. 调整 `GlobalConfigLoader.scala` 中的 `dacClkDiv = n` 即可调整为 `(n+1)` 分频
       3. 实际 DAC 数据的速率比主时钟慢很多，如果不分频不影响的话也可以不分

12. 关于数/模转换

    1. 电压问题
       1. 不清楚 ADC/DAC 使用的电压
       2. 如果直接对接的话可能需要调整发送端或者接收端电压阈值
       3. 调整发送端阈值
          1. 调整 `GlobalConfigLoader.scala:97-99` 的值
          2. 分别表示当发送端需要发送电平为 0/1 的时候对 DAC 的输出
       4. 调整接收端阈值
          1. 在文件夹下输入 `menuconfig` 调整
          2. 或者调整 `GlobalConfigLoader.scala` 中 `THRESHOLD` 的值
          3. 当（以无符号数表示的） `ASKReceiver.io.adc > threshold` 时被判定为 1
    2. 单位问题
       1. ⚠️有些 ADC/DAC 的输入/输出参数不是简单的无符号数
       2. 有可能是第一位表示正/负，后面的位表示当前电压幅度
       3. 例如，对 8-bit ADC 的输出 `8b11111111`
       4. 如果是简单无符号数表示则为 `-1`
       5. 如果是第一位表示正负则为 `-127`
       6. 当前对发送端和接收端默认为使用简单无符号数
    3. 如果不需要数模转换 
       1. 对发送端，可以选择 DAC 输出的任意一位作单线输出（当 `VALUE_1` 被设置为 `0xff` 时）
       2. 对接收端
          1. 如果是使用无符号数，可以将 DAC 输入端口的最高位作为输出
          2. 如果是使用有符号数，可以将 DAC 输入端口的次高位作为输出
          3. 以上操作需要给其余输入位下拉接地，修改 `ASKReciever.xdc`，取消注释其中下拉部分

13. 关于接口

    1. 发送端、接收端

14. 关于调试

    1. 测试发送端是否在工作
       1. 设置 `GlobalConfigLoader.scala` 中的 `def constantAdc = /*false*/ true`
       2. 则将会不读取 ADC 输出而是循环一段固定数据
    2. 发送端自动循环
       1. 设置 `GlobalConfigLoader.scala` 中的 `autoStart = /*false*/ true`
       2. 则发送端将取消 `start` 输入端口并自动重发

15. 关于同步

    1. 目前接收端将会记录每一次信号上升沿

    2. 同步头

       1. 若在规定时间内得到信号包头 `10101011` 即进行时钟周期调整

       2. 格式为

          ```
                 1    0    1    0    1    0    1   1
              ┌────┐    ┌────┐    ┌────┐    ┌─────────
              │    │    │    │    │    │    │ Pre-code
              │    │    │    │    │    │    │
          ────┘    └────┘    └────┘    └────┘
          ```

    3. 每一次包传输都会在包头添加上面的校准

    4. 一个包大小 72 比特，包含 64 比特数据和 8 比特汉明校验，不包含包头的校准数据

    5. 汉明校验暂未测试

16. 关于实际测试
    1. 汉明校验纠错功能未开启

17. 关于理论延迟
    1. 延迟将会是一个包+校验数据长度

### 执行

环境依赖：

1. Windows
   1. python
   2. mingw-w64
2. Linux
   1. python
   2. gcc
   3. verilator

```shell
# 初始化环境，编译文件，生成 Verilog 等
make
# 调用 Vivado 进行综合、实现
# MODULES 模块多于两个时会并行运行
# 生成的项目在 vivado/*
make impl MODULES="Sender Receiver"
```

如果修改了代码，需要重新执行 `make`。